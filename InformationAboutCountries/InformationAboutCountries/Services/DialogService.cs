﻿namespace InformationAboutCountries.Services
{
    using System;
    using Xamarin.Forms;
    using Helpers;
    using System.Threading.Tasks;

    public class DialogService
    {
        public async Task ShowMessage(string title,string Message){
            await Application.Current.MainPage.DisplayAlert(
                title,
                Message,
                Lenguages.Accept);
        }
    }
}
