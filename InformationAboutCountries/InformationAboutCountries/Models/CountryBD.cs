﻿namespace InformationAboutCountries.Models
{
    using SQLite.Net.Attributes;
    class CountryBD
    {
        [PrimaryKey]
        public int CountryID { get; set; }
        public string name { get; set; }
        public string topLevelDomain { get; set; }
        public string flag { get; set; }
        public Translations translations { get; set; }
        public string alpha3Code { get; set; }
        public string capital { get; set; }
        public string borders { get; set; }
        public string nativeName { get; set; }
        public string callingCodes { get; set; }
        public string region { get; set; }
        public double? area { get; set; }
        public long population { get; set; }
        public string timezones { get; set; }
        public string iso639_1 { get; set; }
        public string iso639_2 { get; set; }
        public long latlng { get; set; }
        public string code { get; set; }
        public string symbol { get; set; }

        public override int GetHashCode()
        {
            return CountryID;
        }
    }
}
