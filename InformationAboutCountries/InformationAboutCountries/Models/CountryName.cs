﻿namespace InformationAboutCountries.Models
{
    public class CountryName
    {
        public string Code
        {
            get;
            set;
             
        }

        public string Name
        {
            get;
            set;
        }
    }
}
