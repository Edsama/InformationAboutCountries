﻿using System.Collections.Generic;
namespace InformationAboutCountries.Models
{
    public class Country
    {
        public string name { get; set; }
        public List<string> topLevelDomain { get; set; }
        public string flag { get; set; }
        public Translations translations { get; set; }
        public string alpha3Code { get; set; }
        public string capital { get; set; }
        public List<string> borders { get; set; }
        public string nativeName { get; set; }
        public List<languages> Languages { get; set; }
        public List<string> callingCodes { get; set; }
        public string region { get; set; }
        public double? area { get; set; }
        public long population { get; set; }
        public List<string> timezones { get; set; }
        public List<Currency> currencies { get; set; }
        public List<long> latlng { get; set; }
    }

    public class Translations
    {
        public string de { get; set; }
        public string es { get; set; }
        public string fr { get; set; }
        public string ja { get; set; }
        public string it { get; set; }
        public string br { get; set; }
        public string pt { get; set; }
        public string nl { get; set; }
        public string hr { get; set; }
    }

    public class languages
    {
        public string iso639_1 { get; set; }
        public string iso639_2 { get; set; }
        public string name { get; set; }
        public string nativeName { get; set; }
    }

    public class Currency
    {
        public string code { get; set; }
        public string name { get; set; }
        public string symbol { get; set; }
    }

}
