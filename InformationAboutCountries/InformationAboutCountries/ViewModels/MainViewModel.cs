﻿namespace InformationAboutCountries.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using System;
    using System.ComponentModel;
    using System.Net.Http;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using Services;
    using Xamarin.Forms;
    using System.Linq;
    using Helpers;

    public class MainViewModel: INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Attributes
        bool _isRunning;
        bool _isEnabled;
        string _result;
        string _flag;
        string _capital;
        string _Tcapital;
        string _Tborders;
        string _borders;
        string _name;
        string _Tname;
        string _lenguages;
        string _domain;
        string _Tdomain;
        string _Tlenguages;
        string _code;
        string _Tcode;
        string _consult;
        string _region;
        string _Tregion;
        string _Tpopulation;
        string _Tarea;
        double _area;
        long _population;
        string _Ttimezone;
        string _timezone;
        string _Tmoney;
        string _money;
        string _Tlatlon;
        string _latlon;
        string _information;
        string _PCountry;
        List<CountryName> countryNames;
        List<Country> countries;
        ObservableCollection<CountryName> _countryNames;
        #endregion
        #region Propierties
        public ObservableCollection<CountryName> CountryNames{
            get{
                return _countryNames;
            }
            set{
                if(_countryNames != value){

                    _countryNames = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(CountryNames)));
                }
            }
        }
        ApiService apiService;

        public CountryName CountryName{
            get;
            set;
        }

        public double area
        {
            get
            {
                return _area;
            }
            set
            {
                if (_area != value)
                {
                    _area = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(area)));
                }
            }
        }

        public string Tarea
        {
            get
            {
                return _Tarea;
            }
            set
            {
                if (_Tarea != value)
                {
                    _Tarea = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Tarea)));
                }
            }
        }

        public string Capital{
            get{
                return _capital;
            }
            set{
                if(_capital != value){
                    _capital = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Capital)));
                }
            }
        }

        public string TConsult
        {
            get
            {
                return _consult;
            }
            set
            {
                if (_consult != value)
                {
                    _consult = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(TConsult)));
                }
            }
        }

        public string Region
        {
            get
            {
                return _region;
            }
            set
            {
                if (_region != value)
                {
                    _region = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Region)));
                }
            }
        }

        public string Tregion
        {
            get
            {
                return _Tregion;
            }
            set
            {
                if (_Tregion != value)
                {
                    _Tregion = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Tregion)));
                }
            }
        }

        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (_code != value)
                {
                    _code = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Code)));
                }
            }
        }

        public string TCode
        {
            get
            {
                return _Tcode;
            }
            set
            {
                if (_Tcode != value)
                {
                    _Tcode = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(TCode)));
                }
            }
        }

        public string Latlon
        {
            get{
                return _latlon;
            }
            set{
                if (_latlon != value){
                    _latlon = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Latlon)));
                }
            }
        }

        public string Information
        {
            get
            {
                return _information;
            }
            set
            {
                if (_information != value)
                {
                    _information = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Information)));
                }
            }
        }

        public string Tlatlon
        {
            get
            {
                return _Tlatlon;
            }
            set
            {
                if (_Tlatlon != value)
                {
                    _Tlatlon = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Tlatlon)));
                }
            }
        }

        public string Tpopulation
        {
            get
            {
                return _Tpopulation;
            }
            set
            {
                if (_Tpopulation != value)
                {
                    _Tpopulation = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Tpopulation)));
                }
            }
        }

        public long  population
        {
            get
            {
                return _population;
            }
            set
            {
                if (_population != value)
                {
                    _population = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(population)));
                }
            }
        }



        public string Domain
        {
            get
            {
                return _domain;
            }
            set
            {
                if (_domain != value)
                {
                    _domain = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Domain)));
                }
            }
        }

        public string Tdomain
        {
            get
            {
                return _Tdomain;
            }
            set
            {
                if (_Tdomain != value)
                {
                    _Tdomain = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Tdomain)));
                }
            }
        }

        public string Lenguage
        {
            get
            {
                return _lenguages;
            }
            set
            {
                if (_lenguages != value)
                {
                    _lenguages = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Lenguage)));
                }
            }
        }

        public string Tlenguages
        {
            get
            {
                return _Tlenguages;
            }
            set
            {
                if (_Tlenguages != value)
                {
                    _Tlenguages = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Tlenguages)));
                }
            }
        }

        public string TCapital
        {
            get
            {
                return _Tcapital;
            }
            set
            {
                if (_Tcapital != value)
                {
                    _Tcapital = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(TCapital)));
                }
            }
        }

        public string TBorders
        {
            get
            {
                return _Tborders;
            }
            set
            {
                if (_Tborders != value)
                {
                    _Tborders = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(TBorders)));
                }
            }
        }

        public string Borders
        {
            get
            {
                return _borders;
            }
            set
            {
                if (_borders != value)
                {
                    _borders = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Borders)));
                }
            }
        }

        public string TMoney
        {
            get
            {
                return _Tmoney;
            }
            set
            {
                if (_Tmoney != value)
                {
                    _Tmoney = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(TMoney)));
                }
            }
        }

        public string Money
        {
            get
            {
                return _money;
            }
            set
            {
                if (_money != value)
                {
                    _money = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Money)));
                }
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Name)));
                }
            }
        }

        public string TName
        {
            get
            {
                return _Tname;
            }
            set
            {
                if (_Tname != value)
                {
                    _Tname = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(TName)));
                }
            }
        }

        public string PCountry
        {
            get
            {
                return _PCountry;
            }
            set
            {
                if (_PCountry != value)
                {
                    _PCountry = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(PCountry)));
                }
            }
        }

        public string Ttimezone
        {
            get
            {
                return _Ttimezone;
            }
            set
            {
                if (_Ttimezone != value)
                {
                    _Ttimezone = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Ttimezone)));
                }
            }
        }

        public string timezone
        {
            get
            {
                return _timezone;
            }
            set
            {
                if (_timezone != value)
                {
                    _timezone = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(timezone)));
                }
            }
        }

        public bool IsRunning{
            get{
                return _isRunning;
            }
            set {
                if (_isRunning != value){
                    _isRunning = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(IsRunning)));
                }
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                if (_isEnabled != value)
                {
                    _isEnabled = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(IsEnabled)));
                }
            }
        }

        public bool IsVisible
        {
            get;
            set;
        }

        public string Result
        {
            get{
                return _result;
            }
            set{
                if(_result != value){
                    _result = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Result)));
                }
            }
        }

        public string Flag
        {
            get
            {
                return _flag;
            }
            set
            {
                if (_flag != value)
                {
                    _flag = value;
                    PropertyChanged?.Invoke(this,
                        new PropertyChangedEventArgs(nameof(Flag)));
                }
            }
        }
        #endregion
        #region Constructor
        public MainViewModel()
        {
            apiService = new ApiService();
            LoadCountries();        
        }
        #endregion

        #region Methods
        async void LoadCountries(){
            IsRunning = true;
            Result = Lenguages.Result;
            Tdomain = Lenguages.Domain;
            TCapital = Lenguages.Capital;
            TBorders = Lenguages.Limits;
            Ttimezone = Lenguages.timezone;
            TName = Lenguages.Name;
            Tregion = Lenguages.region;
            TCode = Lenguages.Code;
            Information = Lenguages.Information;
            Tarea = Lenguages.Area;
            Tlatlon = Lenguages.Latitude;
            TMoney = Lenguages.Money;
            Tpopulation = Lenguages.Population;
            PCountry = Lenguages.PCountry;
            TConsult = Lenguages.Consult;
            Tlenguages = Lenguages.Len;
            var conection = await apiService.CheckConnection();
            if (!conection.IsSuccess)
            {
                Result = conection.Message;
                return;
            }
            try
            {
                //var client = new HttpClient();
                //client.BaseAddress = new Uri("http://restcountries.eu");
                //var controller = "/rest/v2/all";
                //var response = await client.GetAsync(controller);
                //var result = await response.Content.ReadAsStringAsync();
                var response = await apiService.GetList<Country>("http://restcountries.eu",
                    "/rest/v2/all");
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    await Application.Current.MainPage.DisplayAlert("Error",
                        response.Message,
                        "Accept");
                    return;
                }

                //var list = JsonConvert.DeserializeObject<List<Country>>(result);
                //Countries = new ObservableCollection<Country>(list);
                countries = (List<Country>)response.Result;

                var ci = Lenguages.Lenguage;
                countryNames = new List<CountryName>();
                foreach(var country in countries){
                    switch (ci){
                        case "en":
                            countryNames.Add(new CountryName
                            {
                                Code = country.alpha3Code,
                                Name = country.name,
                            });
                            break;
                        case "es":
                            countryNames.Add(new CountryName
                            {
                                Code = country.alpha3Code,
                                Name = string.IsNullOrEmpty(country.translations.es) ? country.name : 
                                country.translations.es,
                            });
                            break;

                        case "ja":
                            countryNames.Add(new CountryName
                            {
                                Code = country.alpha3Code,
                                Name = string.IsNullOrEmpty(country.translations.ja) ? country.name :
                                country.translations.ja,
                            });
                            break;
                        case "de":
                            countryNames.Add(new CountryName
                            {
                                Code = country.alpha3Code,
                                Name = string.IsNullOrEmpty(country.translations.de) ? country.name :
                                country.translations.de,
                            });
                            break;
                        case "it":
                            countryNames.Add(new CountryName
                            {
                                Code = country.alpha3Code,
                                Name = string.IsNullOrEmpty(country.translations.it) ? country.name :
                                country.translations.it,
                            });
                            break;
                        case "fr":
                            countryNames.Add(new CountryName
                            {
                                Code = country.alpha3Code,
                                Name = string.IsNullOrEmpty(country.translations.fr) ? country.name :
                                country.translations.fr,
                            });
                            break;
                    }
                }
                CountryNames = new ObservableCollection<CountryName>(countryNames.OrderBy(c=> c.Name).ToList());
                IsRunning = false;
                IsEnabled = true;
                Result = Lenguages.Ready;
            }
            catch (Exception ex)
            {
                IsRunning = false;
                Result = ex.Message;
               
            }
        }
        #endregion 

        #region Commands
        public ICommand ConvertCommand{
            get{
                return new RelayCommand(Consult);
            }
        }

        async void Consult()
        {
            if (CountryName == null)
            {
                await Application.Current.MainPage.DisplayAlert(Lenguages.Error, Lenguages.ValidationCountry,
                    Lenguages.Accept);
                return;

            }
           
            var country = countries
                .Where(c => c.alpha3Code == CountryName.Code)
                .FirstOrDefault();
            Capital = country.capital;
            Region = country.region;
            area = country.area.Value ;
            population = country.population;
            Flag = country.flag.Replace("https", "http");
            var borders = String.Empty;
            foreach(var border in country.borders){
                var countryName = countryNames
                    .Where(c => c.Code == border)
                    .FirstOrDefault();
                borders += String.Format("{0},",countryName.Name);
            }
            if(borders.Length > 2){
                borders = borders.Substring(0, borders.Length -1);
            }
            Borders = borders;
            var lenguages = String.Empty;
            foreach (var lenguage in country.Languages)
            {
                lenguages += String.Format("{0} ({1}),", lenguage.name, lenguage.nativeName);
            }
            if (lenguages.Length > 2)
            {
                lenguages = lenguages.Substring(0, lenguages.Length - 1);
            }
            Borders = borders;
            Lenguage = lenguages;
            var domains = String.Empty;
            foreach (var domain in country.topLevelDomain)
            {
                domains += String.Format("{0},", domain);
            }
            if (domains.Length > 2)
            {
                domains = domains.Substring(0, domains.Length - 1);
            }
            Borders = borders;
            Domain = domains;
            var codes = String.Empty;
            foreach (var code in country.callingCodes)
            {
                codes += String.Format("{0},", code);
            }
            if (codes.Length > 2)
            {
                codes = codes.Substring(0, codes.Length - 1);
            }
            Code = codes;
            var timezones = String.Empty;
            foreach (var timezone in country.timezones)
            {
                timezones += String.Format("{0},", timezone);
            }
            if (timezones.Length > 2)
            {
                timezone = timezones.Substring(0, timezones.Length - 1);
            }
            var monies = String.Empty;
            foreach (var money in country.currencies)
            {
                monies += String.Format("{0} {1}", money.symbol,money.name);
            }
            if (monies.Length > 2)
            {
                Money = monies.Substring(0, monies.Length - 0);
            }
            var Latlons = String.Empty;
            foreach (var atlon in country.latlng)
            {
                Latlons += String.Format("{0}", atlon);
            }
            if (Latlons.Length > 2)
            {
                Latlon = Latlons;
            }
            var ci = Lenguages.Lenguage;
            switch (ci)
            {
                case "en":
                    Name = country.name;
                    break;
                case "es":
                    Name = string.IsNullOrEmpty(country.translations.es) ? country.name :
                            country.translations.es;
                    break;

                case "ja":
                    Name = string.IsNullOrEmpty(country.translations.ja) ? country.name :
                            country.translations.ja;
                    break;

                case "fr":
                    Name = string.IsNullOrEmpty(country.translations.fr) ? country.name :
                            country.translations.fr;
                    break;

                case "it":
                    Name = string.IsNullOrEmpty(country.translations.it) ? country.name :
                            country.translations.it;
                    break;

                case "de":
                    Name = string.IsNullOrEmpty(country.translations.de) ? country.name :
                            country.translations.de;
                    break;
            }
        }
        #endregion
    }
}
