﻿namespace InformationAboutCountries.Helpers
{
    using Xamarin.Forms;
    using Interfaces;
    using Resources;
    public static class Lenguages
    {
        static Lenguages()
        {
            var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            Resource.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocale(ci);
        }

       

        public static string Code
        {
            get { return Resource.Code; }
        }

        public static string Ready
        {
            get { return Resource.Ready; }
        }

        public static string Lenguage
        {
            get { return Resource.Lenguages; }
        }

        public static string Len
        {
            get { return Resource.Lenguage; }
        }

        public static string CheckConnection
        {
            get { return Resource.CheckConnection; }
        }

        public static string Consult
        {
            get { return Resource.Consult; }
        }

        public static string Accept
        {
            get { return Resource.Accept; }
        }

        public static string ValidationCountry
        {
            get { return Resource.ValidationCountry; }
        }

        public static string Error
        {
            get { return Resource.Error; }
        }


        public static string Information
        {
            get { return Resource.InformationAboutCountries; }
        }

        public static string Result
        {
            get { return Resource.Result; }
        }

        public static string PCountry
        {
            get { return Resource.SelectACountry; }
        }

        public static string Area
        {
            get { return Resource.Area; }
        }

        public static string Capital
        {
            get { return Resource.Capital; }
        }

        public static string Domain
        {
            get { return Resource.Domain; }
        }

        public static string Latitude
        {
            get { return Resource.Latitude; }
        }

        public static string Limits
        {
            get { return Resource.Limits; }
        }

        public static string Money
        {
            get { return Resource.Money; }
        }

        public static string Name
        {
            get { return Resource.Name; }
        }

        public static string Population
        {
            get { return Resource.Population; }
        }

        public static string region
        {
            get { return Resource.Region; }
        }

        public static string timezone
        {
            get { return Resource.TimeZone; }
        }

    }
}
