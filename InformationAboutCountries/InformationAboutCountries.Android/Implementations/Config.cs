﻿using Xamarin.Forms;
[assembly: Dependency(typeof(InformationAboutCountries.Droid.Implementations.Config))]
namespace InformationAboutCountries.Droid.Implementations
{

    using Interfaces;
    using SQLite.Net.Interop;
    using Xamarin.Forms;

    public class Config : IConfig
    {
        string directoryDB;
        ISQLitePlatform platform;

        public string DirectoryDB
        {
            get
            {
                if (string.IsNullOrEmpty(directoryDB))
                {
                    directoryDB = System.Environment.GetFolderPath(
                        System.Environment.SpecialFolder.Personal);
                }
                return directoryDB;
            }
        }

        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
                }
                return platform;
            }
        }

    }
}

